import React, {Component} from 'react'
import './App.css'
import VisNetwork from "./VisNetwork";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            graphData: {
                nodes: [
                    {id: -1, label: "Licenciatura en Sistemas"},
                    {id: 0, label: "Introducción a la Programación"},
                    {id: 1, label: "Introducción a la Matemática"},
                    {id: 2, label: "Taller de Lectura y Escritura"},
                    {id: 3, label: "Programación I"},
                    {id: 4, label: "Lógica y Teoría de Números"},
                    {id: 5, label: "Organización del Computador I"},
                    {id: 6, label: "Programación II"},
                    {id: 7, label: "Álgebra Lineal"},
                    {id: 8, label: "Sistemas Operativos y Redes I"},
                    {id: 9, label: "Programación III"},
                    {id: 10, label: "Cálculo para Computación"},
                    {id: 11, label: "Problemas Socioeconómicos Contemporáneos"},
                    {id: 12, label: "Bases de Datos I"},
                    {id: 13, label: "Matemática Discreta"},
                    {id: 14, label: "Especificaciones y Verificación de Software"},
                    {id: 15, label: "Teoría de la Computación"},
                    {id: 16, label: "Ingeniería de Software I"},
                    {id: 17, label: "Probabilidad y Estadística"},
                    {id: 18, label: "Proyecto Profesional I"},
                    {id: 19, label: "Ingeniería de Software II"},
                    {id: 20, label: "Organización del Computador II"},
                    {id: 21, label: "Proyecto Profesional II"},
                    {id: 22, label: "Bases de Datos II"},
                    {id: 23, label: "Sistemas Operativos y Redes II"},
                    {id: 24, label: "Práctica Profesional Supervisada"},
                    {id: 25, label: "Modelado y Optimización"},
                    {id: 26, label: "Informática y Sociedad"},
                    {id: 27, label: "Taller de Tesina de Licenciatura"},
                    {id: 28, label: "Gestión de Proyectos"},
                    {id: 29, label: "Laboratorio Interdisciplinario"},
                    {id: 30, label: "Taller de Utilitarios"},
                    {id: 31, label: "Inglés Lectocomprensión I"},
                    {id: 32, label: "Inglés Lectocomprensión II"},
                    {id: 33, label: "Inglés Lectocomprensión III"},
                    {id: 34, label: "TIC Lectura y Lectoescritura"},
                    {id: 35, label: "TIC Ciencias exactas"},
                    {id: 36, label: "TIC Matemáticas"},
                ],
                edges: [
                    {from: -1, to: 29},
                    {from: -1, to: 30},
                    {from: -1, to: 34},
                    {from: -1, to: 35},
                    {from: -1, to: 36},
                    {from: 0, to: 3},
                    {from: 0, to: 5},
                    {from: 1, to: 4},
                    {from: 1, to: 6},
                    {from: 1, to: 7},
                    {from: 1, to: 10},
                    {from: 2, to: 18},
                    {from: 2, to: 32},
                    {from: 3, to: 6},
                    {from: 3, to: 8},
                    {from: 4, to: 13},
                    {from: 4, to: 12},
                    {from: 4, to: 14},
                    {from: 5, to: 20},
                    {from: 5, to: 8},
                    {from: 5, to: 12},
                    {from: 5, to: 15},
                    {from: 6, to: 9},
                    {from: 6, to: 12},
                    {from: 7, to: 10},
                    {from: 7, to: 13},
                    {from: 8, to: 23},
                    {from: 9, to: 14},
                    {from: 9, to: 15},
                    {from: 9, to: 16},
                    {from: 9, to: 22},
                    {from: 10, to: 13},
                    {from: 10, to: 17},
                    {from: 11, to: 18},
                    {from: 12, to: 18},
                    {from: 12, to: 22},
                    {from: 13, to: 15},
                    {from: 13, to: 17},
                    {from: 14, to: 18},
                    {from: 14, to: 19},
                    {from: 16, to: 18},
                    {from: 16, to: 19},
                    {from: 16, to: 26},
                    {from: 16, to: 18},
                    {from: 17, to: 25},
                    {from: 18, to: 21},
                    {from: 18, to: 28},
                    {from: 19, to: 28},
                    {from: 21, to: 24},
                    {from: 21, to: 27},
                    {from: 22, to: 24},
                    {from: 22, to: 27},
                    {from: 31, to: 32},
                    {from: 32, to: 33},
                    {from: 34, to: 2},
                    {from: 34, to: 3},
                    {from: 34, to: 5},
                    {from: 34, to: 7},
                    {from: 34, to: 11},
                    {from: 34, to: 31},
                    {from: 35, to: 0},
                    {from: 35, to: 1},
                    {from: 36, to: 0},
                    {from: 36, to: 1}
                ]
            }
        };
    }

    componentDidMount() {

    }


    render() {
        return (
            <div className="App">
                <VisNetwork nodes={this.state.graphData.nodes} edges={this.state.graphData.edges}/>
            </div>
        )
    }
}
