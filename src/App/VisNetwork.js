import React, {Component, createRef} from "react";
import {DataSet, Network} from 'vis';

const options = {
    edges: {
        smooth: {
            type: "cubicBezier",
            forceDirection: "vertical",
            roundness: 0.4
        }
    },
    groups: {
        approved: {
            color: {
                border: '#28a745',
                background: '#28a745',
                highlight: {
                    border: '#28a745',
                    background: '#28a745'
                },
                hover: {
                    border: '#28a745',
                    background: '#28a745'
                }
            }
        },
        available: {
            color: {
                border: '#17a2b8',
                background: '#17a2b8',
                highlight: {
                    border: '#17a2b8',
                    background: '#17a2b8'
                },
                hover: {
                    border: '#17a2b8',
                    background: '#17a2b8'
                }
            }
        },
        unavailable: {
            color: {
                border: '#dc3545',
                background: '#dc3545',
                highlight: {
                    border: '#dc3545',
                    background: '#dc3545'
                },
                hover: {
                    border: '#dc3545',
                    background: '#dc3545'
                }
            },
        },
    },
    nodes: {
        shape: "box",
        margin: 10,
        widthConstraint: {
            maximum: 200,
            minimum: 150
        },
        heightConstraint: {
            minimum: 40
        },
        font: {
            color: '#ffffff',
            size: 16, // px
            face: 'arial',
            background: 'none',
            strokeWidth: 0, // px
            strokeColor: '#ffffff',
            align: 'center',
            vadjust: 0,
            bold: {
                color: '#ffffff',
                size: 14, // px
                face: 'arial',
                vadjust: 0,
                mod: 'bold'
            }
        }
    },
    layout: {
        hierarchical: {
            direction: "UD",
            sortMethod: "directed",
        }
    },
    physics: {
        hierarchicalRepulsion: {
            nodeDistance: 200,
            damping: 1
        }
    },
};

class VisNetwork extends Component {

    constructor(props) {
        super(props);
        this.nodesDependencyMap = this.generateNodeDependencyMap(props.edges);
        this.nodesMap = this.generateNodesMap(props.nodes);
        this.calculateLevels(props.nodes, this.nodesMap, this.nodesDependencyMap);
        this.addGroupToNodes(props.nodes, this.nodesMap, this.nodesDependencyMap);
        this.state = {
            nodes: new DataSet(props.nodes),
            edges: new DataSet(props.edges)
        };
        this.appRef = createRef();
    }

    generateNodesMap(nodes) {
        let nodesMap = new Map();
        for (let node of nodes) {
            let id = node.id;
            nodesMap.set(id, node);
        }
        return nodesMap;
    }

    generateNodeDependencyMap(edges) {
        let reverseEdgesMap = new Map();
        for (let edge of edges) {
            let from = edge.from;
            let to = edge.to;
            if (reverseEdgesMap.has(to)) {
                reverseEdgesMap.get(to).push(from);
            } else {
                reverseEdgesMap.set(to, [edge.from]);
            }
        }
        return reverseEdgesMap;
    }

    calculateLevels(nodes, nodesMap, reverseEdgesMap) {
        for (let node of nodes) {
            node.level = this.calculateMaxNodeDepth(nodesMap, reverseEdgesMap, node.id);
        }
    }

    /**
     *
     * @param nodesMap
     * @param nodeDependencyMap
     * @param nodeId
     * @returns {number}
     */
    calculateMaxNodeDepth(nodesMap, nodeDependencyMap, nodeId) {
        if (!(nodesMap instanceof Map)) {
            throw new Error("nodesMap parameter should be an instance of Map")
        }
        if (!(nodeDependencyMap instanceof Map)) {
            throw new Error("nodeDependencyMap parameter should be an instance of Map")
        }
        let parents = [];
        let longestParentDepth = 0;
        if (nodeDependencyMap.has(nodeId)) {
            parents = nodeDependencyMap.get(nodeId);
            for (let parentId of parents) {
                let parentDepth = 1;
                parentDepth += this.calculateMaxNodeDepth(nodesMap, nodeDependencyMap, parentId);
                if (parentDepth > longestParentDepth) {
                    longestParentDepth = parentDepth;
                }
            }
        }
        return longestParentDepth;
    }

    addGroupToNodes(nodes, nodesMap, nodeDependencyMap) {
        if (!(nodesMap instanceof Map)) {
            throw new Error("nodesMap parameter should be an instance of Map")
        }
        if (!(nodeDependencyMap instanceof Map)) {
            throw new Error("nodeDependencyMap parameter should be an instance of Map")
        }
        for (let node of nodes) {
            if ('group' in node && node.group === "approved") {
                continue;
            }
            if (!nodeDependencyMap.has(node.id)) {
                node.group = "approved";
            } else {
                let parentNodesIds = nodeDependencyMap.get(node.id);
                let allApproved = true;
                for (let nodeId of parentNodesIds) {
                    if (nodesMap.has(nodeId) && nodesMap.get(nodeId).group !== "approved") {
                        allApproved = false;
                        break;
                    }
                }
                if (allApproved) {
                    node.group = "available";
                } else {
                    node.group = "unavailable";
                }
            }
        }
    }

    componentDidMount() {
        this.network = new Network(this.appRef.current, {nodes: this.state.nodes, edges: this.state.edges}, options);
        this.network.on("doubleClick", (eventObj) => {
            if (eventObj.nodes.length === 1) {
                let nodeId = eventObj.nodes[0];
                let nodesDataSet = this.state.nodes;
                let nodeData = this.nodesMap.get(nodeId);
                if (nodeData.group === "approved") {
                    nodeData.group = "unavailable"
                } else {
                    if (nodeData.group === "unavailable" || nodeData.group === "available") {
                        nodeData.group = "approved";
                    }
                }
                nodesDataSet.update(nodeData);
                let currentNodes = nodesDataSet.get();
                this.addGroupToNodes(currentNodes, this.nodesMap, this.nodesDependencyMap);
                nodesDataSet.update(currentNodes);
            }
        });
    }

    render() {
        return (
            <div className="graph" ref={this.appRef}/>
        );
    }
}

export default VisNetwork
